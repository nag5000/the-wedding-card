const server = require('server');
const { get, post, error } = server.router;
const { render, send, status } = server.reply;

require('dotenv').config();
const db = require('./db');

server({
  security: { csrf: false }
}, [
  get('/', ctx => render('../public/home.html')),
  get('/rsvp', ctx => render('../public/card.html')),
  get('/api/rsvp', queryRsvp),
  post('/api/rsvp', answerRsvp),
  get(ctx => send('<h1>404</h1><h2>Этой страницы не существует.</h2>')),
  error(ctx => status(500).send(ctx.error.message))
]).then(ctx => {
  console.log(`Server launched on ${ctx.options.port} port`);
});

function queryRsvp(ctx) {
  let id = ctx.query.id;
  if (!id) {
    db.get('log')
      .push({ id: id, event: 'query', error: '400 id is not defined', t: new Date() })
      .write();

    return status(400).send({ error: 'id query param is missing' });
  }

  const people = {
    'demo': { names: 'Василий Петрович и Галина Александровна' },
    'J1yOajIE': { names: 'Кристина' },

    'pS3Qhw1a': { names: 'Светлана Александровна и Андрей Алексеевич' },
    's06tgJ1e': { names: 'Дмитрий, Полина и Тимофей' },
    'TrQ6RvRe': { names: 'Евгения, Иван, Данил и Вика' },
    'uHLNcJl3': { names: 'Роза Михайловна' },
    'EYXR3iwB': { names: 'Ольга Алексеевна , Анатолий Викторович и Максим' },
    'w1IMINpy': { names: 'Александр', _surname: 'Чалин' },
    '0UDEHzia': { names: 'Екатерина и Василий' },
    '1U8tc8ZE': { names: 'Джулия и Алексей' },
    'Y438TQOg': { names: 'Татьяна и Александр' },
    '66AVRDre': { names: 'Вадим' },
    'mrJC5c2l': { names: 'Юлия' },
    'eDORJ3tP': { names: 'Елена и Сергей' },
    'bhyY1VAV': { names: 'Богдан' },

    'TbvFkl9U': { names: 'Александр Витальевич' },
    'ycyiDq0Z': { names: 'Раиса Александровна' },
    'lapGiiPI': { names: 'Андрей, Марина , Екатерина и Мария' },
    'K4gGsiiP': { names: 'Елена и Дмитрий' },
    'AIDcXhLh': { names: 'Вера Александровна' },
    'ykCkpUpD': { names: 'Алексей' },
    '8w6QEFZK': { names: 'Павел и Наталья' },
    'G9cCZgxG': { names: 'Жан' },
    'E0nQBNFn': { names: 'Евгений' },
    'ZZTZofcj': { names: 'Владислав и Людмила' }
  };

  let item = people[id];
  if (!item) {
    db.get('log')
      .push({ id: id, event: 'query', error: '404 id is not found', t: new Date() })
      .write();

    return status(404).send({ error: 'id is not found' });
  }

  db.get('log')
    .push({ id: id, event: 'query', t: new Date() })
    .write();

  return send(item);
}

function answerRsvp(ctx) {
  let id = ctx.data.id;
  let answer = ctx.data.answer;
  db.get('log')
    .push({ id: id, event: 'answer', answer: answer, t: new Date() })
    .write();

  return status(200);
}
