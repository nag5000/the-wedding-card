import './card.scss';

window.onerror = function (msg, url, lineNo, columnNo, error) {
  let label = document.querySelector('.error-label');

  if (msg.toLowerCase().indexOf('script error') > -1) {
    label.innerText = 'Script Error: See Browser Console for Detail';
  } else {
    let message = [
      'Message: ' + msg,
      'URL: ' + url,
      'Line: ' + lineNo,
      'Column: ' + columnNo,
      'Error object: ' + error && JSON.stringify(error)
    ].join(' - ');

    label.innerText = message;
  }

  return false;
};

let usrAgent = navigator.userAgent;
let browserClass;

if (usrAgent.indexOf("Chrome") > -1) {
  browserClass = "chrome";
} else if (usrAgent.indexOf("Safari") > -1) {
  browserClass = "safari";
} else if (usrAgent.indexOf("Firefox") > -1) {
  browserClass = "firefox";
} else if (usrAgent.indexOf("MSIE") > -1) {
  browserClass = "ie";
}

document.body.classList.add('browser-' + browserClass);

let card = document.querySelector('.card');
card.addEventListener('click', function() {
  card.dataset.active = true;
  return false;
});

let onHashChange = function() {
  let hash = location.hash;
  if (hash) {
    document.body.dataset.anchor = hash.replace('#', '');
  } else {
    try {
      delete document.body.dataset.anchor;
    } catch(e) {
      // ios safari bug
      // https://stackoverflow.com/questions/28973603/unable-to-delete-property-on-safari-when-trying-to-delete-dataset-attribute-in
      document.body.removeAttribute('data-anchor');
    }
  }
};

onHashChange();
window.addEventListener('hashchange', onHashChange, false);

function getJSON(url, callback) {
  let xhr = new XMLHttpRequest();
  xhr.open('GET', url);
  xhr.onreadystatechange = function() {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        let data = JSON.parse(xhr.responseText);
        callback(null, data);
      } else {
        callback(true, xhr);
      }
    }
  };
  xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  xhr.send();
  return xhr;
}

function postJSON(url, data, callback) {
  var xhr = new XMLHttpRequest();
  xhr.open('POST', url);
  xhr.onreadystatechange = function() {
    if (xhr.readyState === 4) {
      if (xhr.status === 200) {
        callback(null, xhr.responseText);
      } else {
        callback(true, xhr);
      }
    }
  };
  xhr.setRequestHeader('X-Requested-With', 'XMLHttpRequest');
  xhr.setRequestHeader('Content-Type', 'application/json; charset=utf-8');
  xhr.send(JSON.stringify(data));
  return xhr;
}

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

function renderError(message) {
  document.body.classList.add('error');

  if (message) {
    let label = document.querySelector('.error-label');
    label.innerText = message;
  }
}

let queryParamId = getParameterByName('id');
if (queryParamId) {
  getJSON('/api/rsvp?id=' + queryParamId, function(err, data) {
    if (err) {
      let xhr = data;
      if (xhr.status == 404) {
        renderError(
          `Ваш идентификатор ("${queryParamId}") не найден в базе`
            + ` зарегистрированных гостей. Проверьте адрес текущей страницы.`
        );
      } else {
        renderError(
          'Произошла ошибка. Это могут быть проблемы с сетью,'
            + ' проверьте ваше интернет соединение.'
            + ' Либо сервис недоступен сейчас, попробуйте зайти позже.'
        );
      }
    } else {
      let namesEl = document.getElementById('names');
      namesEl.innerText = data.names;

      if (data.names.length < 22) {
        namesEl.dataset.large = true;
      }

      try {
        delete namesEl.dataset.loading;
      } catch(e) {
        // ios safari bug
        // https://stackoverflow.com/questions/28973603/unable-to-delete-property-on-safari-when-trying-to-delete-dataset-attribute-in
        namesEl.removeAttribute('data-loading');
      }
    }
  });
} else {
  renderError(
    'Неверный адрес. Не указан идентификатор гостя.'
      + ' Проверьте адрес текущей страницы.'
  );
}

let btnAccept = document.getElementById('btn-accept');
btnAccept.addEventListener('click', function() {
  btnAccept.dataset.loading = true;
  postJSON('/api/rsvp', { id: queryParamId, answer: true }, function(err, data) {
    try {
      delete btnAccept.dataset.loading;
    } catch(e) {
      // ios safari bug
      // https://stackoverflow.com/questions/28973603/unable-to-delete-property-on-safari-when-trying-to-delete-dataset-attribute-in
      btnAccept.removeAttribute('data-loading');
    }

    if (err) {
      alert(
        'Произошла ошибка, ваш ответ не был отправлен :(.'
          + ' Это могут быть проблемы с сетью,'
          + ' проверьте ваше интернет соединение.'
          + ' Либо сервис недоступен сейчас, попробуйте зайти позже.'
      );
    } else {
      alert('Ответ отправлен. Спасибо, ждем вас с нетерпением! ^_^');
    }
  });
});

let btnReject = document.getElementById('btn-reject');
btnReject.addEventListener('click', function() {
  btnReject.dataset.loading = true;
  postJSON('/api/rsvp', { id: queryParamId, answer: false }, function(err, data) {
    try {
      delete btnReject.dataset.loading;
    } catch(e) {
      // ios safari bug
      // https://stackoverflow.com/questions/28973603/unable-to-delete-property-on-safari-when-trying-to-delete-dataset-attribute-in
      btnReject.removeAttribute('data-loading');
    }

    if (err) {
      alert(
        'Произошла ошибка, ваш ответ не был отправлен :(.'
          + ' Это могут быть проблемы с сетью,'
          + ' проверьте ваше интернет соединение.'
          + ' Либо сервис недоступен сейчас, попробуйте зайти позже.'
      );
    } else {
      alert('Ответ отправлен. Будем надеяться, что вы передумаете :).');
    }
  });
});
